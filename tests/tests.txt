######################
##### TEST TEMPLATE #####
######################
# Activation
# input: Text
# input: Gram selector
# input: N
# input: Case sensitive
# input: Delimiter sensitive
# output: Required result

######################
###### REAL TESTS #######
######################
# Activation
1
# input: Text
Нат пад жалобнай вопраткай чорнай Ты адгадаеш: стан непакорны, Ножкі, якімі б на карнавалах I захапляла і чаравала, Смуглыя рукі, грудзі тугія,— Ave Maria!
# input: Gram selector
character
# input: N
3
# input: Case sensitive
false
# input: Delimiter sensitive
false
# output: Required result
{"ай ":3,"і, ":2,"най":2,"орн":2,"ава":2,"вал":2,"ала":2,"рна":2,"кі,":2,"ла ":1,"а і":1," і ":1,"і ч":1,"Нат":1,"лял":1," ча":1,"чар":1,"ара":1,"рав":1,"ла,":1,"а, ":1,"яла":1,"аха":1,"пля":1,"апл":1,"хап":1," См":1,"зах":1," за":1,"I з":1," I ":1,"х I":1,"ах ":1,"лах":1,"нав":1,"арн":1,"кар":1,", С":1,"угл":1,"Сму":1," Av":1,"угі":1,"гія":1,"ія,":1,"я,—":1,",— ":1,"— A":1,"Ave":1," ту":1,"ve ":1,"e M":1," Ma":1,"Mar":1,"ari":1,"ria":1,"туг":1,"і т":1,"муг":1,"рук":1,"а к":1,"глы":1,"лыя":1,"ыя ":1,"я р":1," ру":1,"укі":1,"зі ":1,", г":1," гр":1,"гру":1,"руд":1,"удз":1,"дзі":1," ка":1," б ":1,"на ":1,"пра":1,"дга":1,"адг":1," ад":1,"ы а":1,"Ты ":1," Ты":1,"й Т":1,"чор":1," чо":1,"й ч":1,"кай":1,"тка":1,"атк":1,"рат":1,"опр":1,"ада":1,"воп":1," во":1,"й в":1,"бна":1,"обн":1,"лоб":1,"ало":1,"жал":1," жа":1,"д ж":1,"ад ":1,"пад":1," па":1,"т п":1,"гад":1,"дае":1," на":1,"ы, ":1,"б н":1,"ат ":1,"і б":1,"мі ":1,"імі":1,"кім":1,"які":1," як":1,", я":1,"жкі":1,"ожк":1,"Нож":1," Но":1,", Н":1,"ны,":1,"аеш":1,"ан ":1,"еш:":1,"ш: ":1,": с":1," ст":1,"ста":1,"тан":1,"н н":1,"рны":1," не":1,"неп":1,"епа":1,"пак":1,"ако":1,"кор":1,"ia!":1}

# Activation
1
# input: Text
груша ГРуша ГрУШа ГРУША груША гРуШа ГРУШа грушА
# input: Gram selector
word
# input: N
1
# input: Case sensitive
true
# input: Delimiter sensitive
true
# output: Required result
{"груша":1,"ГРуша":1,"ГрУШа":1,"ГРУША":1,"груША":1,"гРуШа":1,"ГРУШа":1,"грушА":1}

# Activation
1
# input: Text
Depuis quand le métro existe-t-il? Pourquoi fête-t-on les anniversaires? C’est quoi les jardins à la française?
character
# input: N
4
# input: Case sensitive
false
# input: Delimiter sensitive
false
# output: Required result
{" les":2,"e-t-":2,"uoi ":2,"quoi":2,"te-t":2,"les ":2,"ires":1,"’est":1,"C’es":1," C’e":1,"? C’":1,"s? C":1,"es? ":1,"res?":1,"çais":1,"aise":1,"aire":1,"st q":1,"sair":1,"rsai":1,"ersa":1,"vers":1,"iver":1,"nive":1,"nniv":1,"anni":1," ann":1,"est ":1,"ança":1,"ranç":1,"ins ":1,"fran":1," fra":1,"a fr":1,"la f":1," la ":1,"à la":1," à l":1,"s à ":1,"ns à":1,"dins":1," quo":1,"rdin":1,"ardi":1,"jard":1," jar":1,"s ja":1,"es j":1,"i le":1,"oi l":1,"s an":1,"nçai":1,"t qu":1,"Depu":1,"es a":1," le ":1," exi":1,"o ex":1,"ro e":1,"tro ":1,"étro":1,"métr":1," mét":1,"e mé":1,"le m":1,"d le":1,"xist":1,"nd l":1,"and ":1,"uand":1,"quan":1," qua":1,"s qu":1,"is q":1,"uis ":1,"puis":1,"exis":1,"iste":1,"epui":1,"oi f":1,"n le":1,"on l":1,"-on ":1,"t-on":1,"-t-o":1,"ête-":1,"fête":1," fêt":1,"i fê":1,"rquo":1,"ste-":1,"urqu":1,"ourq":1,"Pour":1," Pou":1,"? Po":1,"l? P":1,"il? ":1,"-il?":1,"t-il":1,"-t-i":1,"ise?":1}

# Activation
1
# input: Text
Für das neue Jahr werden wir eine Menge Salate, warme und Sandwiches mit Kaviar Vorbereitung. Silvester die ganze Familie, die wir an den Präsidenten Rede hören.
# input: Gram selector
character
# input: N
2
# input: Case sensitive
true
# input: Delimiter sensitive
false
# output: Required result
{"e ":8,"r ":6,"en":6,"de":4," d":4," w":4,"n ":4,"ie":3,"wi":3,"te":3,"er":3," S":3,"an":3,"ir":2,"Sa":2,"ei":2,"di":2,"ar":2,"es":2,"un":2,"il":2,"nd":2,"mi":2,"it":2,"ne":2,"e,":2,"s ":2,"re":2,", ":2,"ng":2," g":1,"Fü":1,"st":1,"ve":1,"lv":1,"Si":1,". ":1,"g.":1,"tu":1,"be":1,"ga":1,"am":1,"nz":1,"si":1,"ör":1,"hö":1," h":1,"ed":1,"Re":1," R":1,"nt":1,"id":1,"äs":1,"ze":1,"rä":1,"Pr":1," P":1," a":1,"li":1,"or":1,"Fa":1," F":1,"rb":1,"ch":1,"Vo":1,"hr":1,"Me":1," M":1,"in":1," e":1,"rd":1,"we":1,"ah":1,"al":1,"Ja":1," J":1,"ue":1,"eu":1," n":1,"as":1,"da":1,"ge":1,"la":1," V":1,"he":1,"ia":1,"vi":1,"av":1,"Ka":1," K":1,"t ":1," m":1,"ür":1,"at":1,"ic":1,"dw":1,"d ":1," u":1,"me":1,"rm":1,"wa":1,"n.":1}

# Activation
1
# input: Text
Good Morning, good Morning good Morning to you, good Morning, Good Morning, I am glad to see you!
# input: Gram selector
word
# input: N
2
# input: Case sensitive
false
# input: Delimiter sensitive
true
# output: Required result
{"good Morning":3,"Good Morning":2,"Morning, good":1,"Morning good":1,"Morning to":1,"to you":1,"you, good":1,"Morning, Good":1,"Morning, I":1,"I am":1,"am glad":1,"glad to":1,"to see":1,"see you":1}

# Activation
1
# input: Text
Если у Вас нет собаки, Её не отравит сосед, И с другом не будет драки, Если у Вас, Если у Вас, Если у Вас друга нет, друга нет.
# input: Gram selector
word
# input: N
3
# input: Case sensitive
true
# input: Delimiter sensitive
false
# output: Required result
{"Если у Вас":4,"Вас, Если у":2,"у Вас, Если":2,"другом не будет":1,"друга нет, друга":1,"Вас друга нет":1,"у Вас друга":1,"драки, Если у":1,"будет драки, Если":1,"не будет драки":1,"с другом не":1,"у Вас нет":1,"И с другом":1,"сосед, И с":1,"отравит сосед, И":1,"не отравит сосед":1,"Её не отравит":1,"собаки, Её не":1,"нет собаки, Её":1,"Вас нет собаки":1,"нет, друга нет":1}

# Activation
1
# input: Text
Тэст іў'
# input: Gram selector
character
# input: N
2
# input: Case sensitive
true
# input: Delimiter sensitive
true
# output: Required result
{"Тэ":1,"эс":1,"ст":1,"т ":1," і":1,"іў":1,"ў'":1}

# Activation
1
# input: Text
Груша цвіла апошні год.
# input: Gram selector
character
# input: N
3
# input: Case sensitive
false
# input: Delimiter sensitive
false
# output: Required result
{"Гру":1," ап":1,"год":1," го":1,"і г":1,"ні ":1,"шні":1,"ошн":1,"пош":1,"апо":1,"а а":1,"руш":1,"ла ":1,"іла":1,"віл":1,"цві":1," цв":1,"а ц":1,"ша ":1,"уша":1,"од.":1}

# Activation
1
# input: Text
It is a truth unіversally acknowledged, that a single man in possesssion of a good fortune must be in want of a wife.
# input: Gram selector
word
# input: N
2
# input: Case sensitive
false
# input: Delimiter sensitive
false
# output: Required result
{"of a":2,"It is":1,"possesssion of":1,"want of":1,"in want":1,"be in":1,"must be":1,"fortune must":1,"good fortune":1,"a good":1,"in possesssion":1,"is a":1,"man in":1,"single man":1,"a single":1,"that a":1,"acknowledged, that":1,"unіversally acknowledged":1,"truth unіversally":1,"a truth":1,"a wife":1}

# Activation
1
# input: Text
no more cola no more pizza all we need is Slivovitza!
# input: Gram selector
word
# input: N
2
# input: Case sensitive
true
# input: Delimiter sensitive
true
# output: Required result
{"no more":2,"more cola":1,"cola no":1,"more pizza":1,"pizza all":1,"all we":1,"we need":1,"need is":1,"is Slivovitza":1}	
