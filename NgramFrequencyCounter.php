<?php
	class NgramFrequencyCounter {
		private static $localizationArr = array();
		private static $localizationErr = array();
		private $text = '';
		private $n = '';
		private $gram = '';
		private $caseSensitive = '';
		private $delimiterSensitive = '';
		private $resultHeadline = '';
		private $result = '';
		private $resultArr = array();
		/* characters and const */
		private $apostrophesArr = array("'", "ʼ", "’", "‘");
		private $acuteAccentsArr = array("´", "\xcc\x81");
		private $graveAccent = "\xcc\x80";
		private $letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуфхцчшщъыьэюяЂЃѓЉЊЌЋЏђљњќћџЎўЈҐЁЄЇІіґёєјЅѕї';
		const BR = "<br>\n";

		function __construct() {
		
		}

		public function setText($text) {
			$this->text = $text;
		}
		
		public function setN($n) {
			$this->n = intval($n);
		}
		
		public function setGram($gram) {
			$this->gram = $gram;
		}
		
		public function setCaseSensitive($caseSensitive) {
			$this->caseSensitive = $caseSensitive;
		}
		
		public function setDelimiterSensitive($delimiterSensitive) {
			$this->delimiterSensitive = $delimiterSensitive;
		}
		
		public static function loadLanguages() {
			$languages = array();
			$files = scandir('lang/');
			if(!empty($files)) {
				foreach($files as $file) {
					if(substr($file, 2, 4) == '.txt') {
						$languages[] = substr($file, 0, 2);
					}
				}
			}
			return $languages;
		}
		
		public static function loadLocalization($lang) {
			$filepath = "lang/$lang.txt";
			$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			if($linesArr === false) {
				$filepath = "lang/en.txt";
				$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			}
			foreach($linesArr as $line) {
				if(empty($line)) {
					$key = $value = '';
				}
				elseif(substr($line, 0, 1) !== '#' && substr($line, 0, 2) !== '//') {
					if(empty($key)) {
						$key = $line;
					}
					else {
						if(!isset(self::$localizationArr[$key])) {
							self::$localizationArr[$key] = $line;
						}
					}
				}
			}
		}
		
		public static function showMessage($msg) {
			if(isset(self::$localizationArr[$msg])) {
				return self::$localizationArr[$msg];
			}
			else {
				self::$localizationErr[] = $msg;
				return $msg;
			}
		}
		
		public static function sendErrorList($lang) {
			if(!empty(self::$localizationErr)) {
				$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
				$sendersName = 'N-gram Frequency Counter';
				$recipient = 'corpus.by@gmail.com';
				$subject = "ERROR: Incorrect localization in $sendersName by user $ip";
				$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
				$mailBody .= $_SERVER['HTTP_HOST'] . '/NgramFrequencyCounter/ дасылае інфармацыю аб наступных памылках:' . self::BR . self::BR;
				$mailBody .= "Мова лакалізацыі: <b>$lang</b>" . self::BR;
				$mailBody .= 'Адсутнічае лакалізацыя наступных ідэнтыфікатараў:' . self::BR . implode(self::BR, self::$localizationErr) . self::BR;
				$header  = "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html; charset=utf-8\r\n";
				$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
				mail($recipient, $subject, $mailBody, $header);
			}
		}
				
		private function defineWords($string, $characterGroup1, $characterGroup2) {
			$wordsArr = array();
			if(!empty($characterGroup1)) {
				$pattern = "/(^|[$characterGroup1][$characterGroup1$characterGroup2]*)([^$characterGroup1]*)/u";
				preg_match_all($pattern, $string, $wordsArr, PREG_SET_ORDER);
			}
			return $wordsArr;
		}
		
		public function run() {
			$gramCnt = 0;
			$resultArr = array();
			$text = (!$this->caseSensitive) ? mb_strtolower($this->text, 'UTF-8') : $this->text;
			if($this->gram == 'character') {
				if(!$this->delimiterSensitive) {
					$text = str_replace(' ', '', $text);
				}
				$gramArr = preg_split('//u', $text, -1, PREG_SPLIT_NO_EMPTY);
				$gramCnt = mb_strlen($text, "UTF-8");
				for($i = 0; $i <= $gramCnt - $this->n; $i++) {
					$ngram = '';
					for($j = 0; $j < $this->n; $j++) {
						$ngram .= $gramArr[$i+$j];
					}
					if(isset($resultArr[$ngram])) {
						$resultArr[$ngram]++;
					}
					else {
						$resultArr[$ngram] = 1;
					}
				}
			}
			elseif($this->gram == 'word') {
				$characterGroup1 = $this->letters;
				$characterGroup2 = implode('', $this->acuteAccentsArr) . implode('', $this->apostrophesArr) . $this->graveAccent . '\-';
				$gramArr = $this->defineWords($text, $characterGroup1, $characterGroup2);
				$gramCnt = count($gramArr);
				for($i = 0; $i <= $gramCnt - $this->n; $i++) {
					if(empty($gramArr[$i][1])) {
						continue;
					}
					$ngram = '';
					for($j = 0; $j < $this->n; $j++) {
						if($this->delimiterSensitive) {
							$ngram .= $gramArr[$i+$j][1];
							if($j+1 < $this->n) {
								$ngram .= $gramArr[$i+$j][2];
							}
						}
						else {
							$ngram .= $gramArr[$i+$j][1];
							if($j+1 < $this->n) {
								$ngram .= ' ';
							}
						}
					}
					if(isset($resultArr[$ngram])) {
						$resultArr[$ngram]++;
					}
					else {
						$resultArr[$ngram] = 1;
					}
				}
			}
			
			$this->resultHeadline .= self::$localizationArr['result headline'] . ": <b>$gramCnt</b>" . self::BR;
			if($gramCnt < $this->n) {
				$this->resultHeadline .= "<b>N</b> " . self::$localizationArr['incorrect n'] . self::BR;
				return false;
			}
			$uniqueNgramCnt = count($resultArr);
			arsort($resultArr);
			//ksort($resultArr);
			foreach($resultArr as $ngram => $cnt) {
				$this->result .= "{$cnt}\t{$ngram}\n";
			}
			$this->resultHeadline .= self::$localizationArr['result headline 2'] . " <b>{$this->n}-" . self::$localizationArr['grams'] . "</b>: <b>$uniqueNgramCnt</b>" . self::BR;
			$this->resultArr = $resultArr;
			return true;
		}
		
		public function saveCacheFiles() {
			mb_internal_encoding('UTF-8');
			mb_regex_encoding('UTF-8');
			
			$dateCode = date('Y-m-d_H-i-s', time());
			$randCode = rand(0, 1000);
			$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
			
			$root = $_SERVER['HTTP_HOST'];
			if($root == 'corpus.by') $root = 'https://corpus.by';
			$serviceName = 'NgramFrequencyCounter';
			$sendersName = 'N-gram Frequency Counter';
			$recipient = 'corpus.by@gmail.com';
			$subject = "$sendersName from IP $ip";
			$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
			$mailBody .= "$root/$serviceName/ дасылае інфармацыю аб актыўнасці карыстальніка з IP $ip." . self::BR . self::BR;
			$textLength = mb_strlen($this->text);
			$pages = round($textLength/2300, 1);
			
			$cachePath = dirname(dirname(__FILE__)) . "/_cache";
			if(!file_exists($cachePath)) mkdir($cachePath);
			$cachePath = "$cachePath/$serviceName";
			if(!file_exists($cachePath)) mkdir($cachePath);
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_in.txt';
			$path = "$cachePath/in/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			$cacheText = preg_replace("/(^\s+)|(\s+$)/us", '', $this->text);
			fwrite($newFile, $cacheText);
			fclose($newFile);
			$url = $root . "/showCache.php?s=$serviceName&t=in&f=$filename";
			if(mb_strlen($cacheText)) {
				$mailBody .= "Тэкст ($textLength сімв., прыкладна $pages ст. па 2300 сімв. на старонку) пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cacheText, $matches);
				$str = str_replace("\n", self::BR, trim($matches[0]));
				if(mb_strlen($str) < 300) {
					$mailBody .= '<blockquote><i>' . $str . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				}
				else {
					$mailBody .= '<blockquote><i>' . mb_substr($str, 0, 300) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				}
			}
			$mailBody .= self::BR;
			
			$mailBody .= $this->resultHeadline . self::BR . self::BR;
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_out.txt';
			$path = "$cachePath/out/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			$cacheText = preg_replace("/(^\s+)|(\s+$)/us", "", $this->result);
			fwrite($newFile, $cacheText);
			fclose($newFile);
			$url = $root . "/showCache.php?s=$serviceName&t=out&f=$filename";
			if(mb_strlen($cacheText)) {
				$mailBody .= "Вынік пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cacheText, $matches);
				$mailBody .= '<blockquote><i>' . str_replace("\n", self::BR, trim($matches[0])) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
			}
			$mailBody .= self::BR;
			
			$header  = "MIME-Version: 1.0\r\n";
			$header .= "Content-type: text/html; charset=utf-8\r\n";
			$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
			mail($recipient, $subject, $mailBody, $header);
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_e.txt';
			$path = "$cachePath/email/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			fwrite($newFile, join("\n", array("recipient: \t" . $recipient, "subject: \t" . $subject, "\n\tMAIL BODY\n\n" . $mailBody, $header)));
			fclose($newFile);
		}
		
		public function getResultHeadline() { return $this->resultHeadline; }
		public function getResult() { return $this->result; }
		public function getResultArr() { return $this->resultArr; }
	}
?>