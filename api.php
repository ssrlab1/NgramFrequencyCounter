<?php
	header("Content-type: text/html;  charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
	
	include_once 'NgramFrequencyCounter.php';
	$localization = isset($_POST['localization']) ? $_POST['localization'] : 'en';
	NgramFrequencyCounter::loadLocalization($localization);
	
	$text = isset($_POST['text']) ? $_POST['text'] : '';
	$gramSelector = isset($_POST['gramSelector']) ? $_POST['gramSelector'] : 'character';
	$n = isset($_POST['n']) ? $_POST['n'] : '3';
	$caseSensitive = isset($_POST['caseSensitive']) ? '1' : '0';
	$delimiterSensitive = isset($_POST['delimiterSensitive']) ? '1' : '0';
	
	$msg = '';
	if(!empty($text)) {
		$NgramFrequencyCounter = new NgramFrequencyCounter();
		$NgramFrequencyCounter->setText($text);
		$NgramFrequencyCounter->setN($n);
		$NgramFrequencyCounter->setGram($gramSelector);
		$NgramFrequencyCounter->setCaseSensitive($caseSensitive);
		$NgramFrequencyCounter->setDelimiterSensitive($delimiterSensitive);
		$NgramFrequencyCounter->run();
		$NgramFrequencyCounter->saveCacheFiles();
		
		$result['text'] = $text;
		$result['result'] = $NgramFrequencyCounter->getResult();
		$result['resultArr'] = $NgramFrequencyCounter->getResultArr();
		$result['headline'] = $NgramFrequencyCounter->getResultHeadline();
		$msg = json_encode($result);
	}
	echo $msg;
?>