<?php
	header("Content-Type: text/html; charset=utf-8");
	$ini = parse_ini_file('service.ini');
	include_once 'NgramFrequencyCounter.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	$languages = NgramFrequencyCounter::loadLanguages();
	NgramFrequencyCounter::loadLocalization($lang);
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
	<head>
		<title><?php echo NgramFrequencyCounter::showMessage('title'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href='css/theme.css'>
		<link rel="icon" type="image/x-icon" href="img/favicon.ico">
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		<?php include_once 'analyticstracking.php'; ?>
		<script>
			var inputTextDefault = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', NgramFrequencyCounter::showMessage('default input'))); ?>";
			function ajaxRequest() {
				var caseSensitive = $('input#caseSensitiveId').prop('checked') == true ? 1 : 0;
				var delimiterSensitive = $('input#delimiterSensitiveId').prop('checked') == true ? 1 : 0;
				$.ajax({
					type: 'POST',
					url: 'https://corpus.by/NgramFrequencyCounter/api.php',
					data: {
						'localization': '<?php echo $lang; ?>',
						'text': $('textarea#inputTextId').val(),
						'gramSelector': $('select#gramSelectorId').val(),
						'n': $('input#nId').val(),
						'caseSensitive': caseSensitive,
						'delimiterSensitive': delimiterSensitive
					},
					success: function(msg) {
						var result = jQuery.parseJSON(msg);
						$('#statisticsId').html(result.headline);
						var output = '';
						if($('input#tableViewId').prop('checked')) {
							output += '<table id="resultTableId" class="table table-sm table-striped">';
							output += '<thead><tr><th scope="col">Frequency</th><th scope="col">Entry</th></tr></thead>';
							output += '<tbody>';
							for(var key in result.resultArr) {
								output += '<tr><td>' + result.resultArr[key] + '</td><td>' + key + '</td></tr>';
							}
						}
						else {
							output += '<textarea class="form-control" rows="10" id="SyllabifierResultId" placeholder="Processed text" readonly>' + result.result + '</textarea>';
						}
						$('#resultId').html(output);
					},
					error: function() {
						$('#resultId').html('ERROR');
					}
				});
			}
			$(document).ready(function () {
				$('button#MainButtonId').click(function() {
					$('#resultId').empty();
					$('#resultId').prepend($('<img>', { src: "img/loading.gif"}));
					$('#resultBlockId').show('slow');
					ajaxRequest();				
				});
				$(document).on('keypress', 'input#nId', function (e) {
					if (e.which == 13) {
						$('#resultId').empty();
						$('#resultId').prepend($('<img>', { src: "img/loading.gif"}));
						$('#resultBlockId').show('slow');
						ajaxRequest();
						return false;
					}
				});
				$(document).on('click', '#copyButtonId', function() {
					copyToClipboard(document.getElementById("resultTableId"));
				});
			});
			
			function copyToClipboard(elem) {
				// create hidden text element, if it doesn't already exist
				var targetId = "_hiddenCopyText_";
				var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
				var origSelectionStart, origSelectionEnd;
				if (isInput) {
					// can just use the original source element for the selection and copy
					target = elem;
					origSelectionStart = elem.selectionStart;
					origSelectionEnd = elem.selectionEnd;
				} else {
					// must use a temporary form element for the selection and copy
					target = document.getElementById(targetId);
					if (!target) {
						var target = document.createElement("textarea");
						target.style.position = "absolute";
						target.style.left = "-9999px";
						target.style.top = "0";
						target.id = targetId;
						document.body.appendChild(target);
					}
					if(elem.tagName === "TABLE") {
						target.textContent = '';
						var table = document.getElementById("resultTableId");
						for (var r = 0, n = table.rows.length; r < n; r++) {
							for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
								target.textContent += table.rows[r].cells[c].firstChild.textContent + "\t";
							}
							target.textContent += "\n";
						}
					}
					else {
						target.textContent = elem.textContent;
					}
				}
				// select the content
				var currentFocus = document.activeElement;
				target.focus();
				target.setSelectionRange(0, target.value.length);
				
				// copy the selection
				var succeed;
				try {
					  succeed = document.execCommand("copy");
				} catch(e) {
					succeed = false;
				}
				// restore original focus
				if (currentFocus && typeof currentFocus.focus === "function") {
					currentFocus.focus();
				}
				
				if (isInput) {
					// restore prior selection
					elem.setSelectionRange(origSelectionStart, origSelectionEnd);
				} else {
					// clear temporary content
					target.textContent = "";
				}
				return succeed;
			}
		</script>
	</head>
	<body>
		<!-- Novigation -->
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"><img style="max-width:150px; margin-top: -7px;" src="img/main-logo.png" alt=""></a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="service-name"><a href="/NgramFrequencyCounter/?lang=<?php echo $lang; ?>"><?php echo NgramFrequencyCounter::showMessage('title'); ?></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="service-name"><a href="<?php echo NgramFrequencyCounter::showMessage('help'); ?>" target="_blank">?</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo NgramFrequencyCounter::showMessage($lang); ?><span class="caret"></span></a>
							<ul class="dropdown-menu">
								<?php
									$languages = NgramFrequencyCounter::loadLanguages();
									foreach($languages as $language) {
										echo "<li><a href='?lang=$language'>" . NgramFrequencyCounter::showMessage($language) . "</a></li>";
									}
								?>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- End of Novigation -->
		<div class="container theme-showcase" role="main">
			<div class="row">
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value=inputTextDefault;"><?php echo NgramFrequencyCounter::showMessage('update'); ?></button>
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value='';"><?php echo NgramFrequencyCounter::showMessage('clear'); ?></button>
								</div>
								<h3 class="panel-title"><?php echo NgramFrequencyCounter::showMessage('input'); ?></h3>
							</div>
							<div class="panel-body">
								<p><textarea class="form-control" rows="10" id = "inputTextId" name="inputText" placeholder="Enter text"><?php echo str_replace('\n', "\n", NgramFrequencyCounter::showMessage('default input')); ?></textarea></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-6">
						<span class="bold"><?php echo NgramFrequencyCounter::showMessage('unit'); ?>:&nbsp;</span>
						<select id="gramSelectorId" name='gramSelector' class="selector-primary">
							<option value='character'><?php echo NgramFrequencyCounter::showMessage('character'); ?></option>
							<!--<option value='token' disabled="disabled"><?php //echo NgramFrequencyCounter::showMessage('token'); ?></option>-->
							<option value='word'><?php echo NgramFrequencyCounter::showMessage('word'); ?></option>
							<!--<option value='regexp' disabled="disabled"><?php //echo NgramFrequencyCounter::showMessage('regexp'); ?></option>-->
						</select>
						<span class="bold">&nbsp;&nbsp;N:&nbsp;</span>
						<input type="text" id="nId" name="n" value="3" class="input-primary">
					</div>
					<div class="col-md-6">
						<input type="checkbox" id="caseSensitiveId" name="caseSensitive" value="1" checked>
						<label for="caseSensitiveId">
							<?php echo NgramFrequencyCounter::showMessage('case sensitive'); ?>
						</label><br />
						<input type="checkbox" id="delimiterSensitiveId" name="delimiterSensitive" value="1" checked>
						<label for="delimiterSensitiveId">
							<?php echo NgramFrequencyCounter::showMessage('delimiters consideration'); ?>
						</label><br />
						<input type="checkbox" id="tableViewId" name="tableView" value="1" checked>
						<label for="tableViewId">
							<?php echo NgramFrequencyCounter::showMessage('table view'); ?>
						</label><br />
					</div>
				</div>
				<div class="col-md-12">
					<button type="button" id="MainButtonId" name="MainButton" class="button-primary"><?php echo NgramFrequencyCounter::showMessage('button'); ?></button>
				</div>
				<div class="col-md-12" id="resultBlockId" style="display: none;">
					<p id="statisticsId"></p>
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="btn-group pull-right">
									<button type="button" id="copyButtonId" class="btn btn-default btn-xs"><?php echo NgramFrequencyCounter::showMessage('copy'); ?></button>
								</div>
								<h3 class="panel-title"><?php echo NgramFrequencyCounter::showMessage('result'); ?></h3>
							</div>
							<div class="panel-body">
								<p id="resultId"></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-body">
								<?php echo NgramFrequencyCounter::showMessage('service code'); ?>&nbsp;<a href="https://gitlab.com/ssrlab1/NgramFrequencyCounter" target="_blank"><?php echo NgramFrequencyCounter::showMessage('reference'); ?></a>.
								<br />
								<a href="https://gitlab.com/ssrlab1/NgramFrequencyCounter/-/issues/new" target="_blank"><?php echo NgramFrequencyCounter::showMessage('suggestions'); ?></a>.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="footer">
			<div class="container">
				<p class="text-muted">
					<?php echo NgramFrequencyCounter::showMessage('contact e-mail'); ?>
					<a href="mailto:corpus.by@gmail.com">corpus.by@gmail.com</a>.<br />
					<?php echo NgramFrequencyCounter::showMessage('other prototypes'); ?>
					<a href="https://corpus.by/?lang=<?php echo $lang; ?>">corpus.by</a>,&nbsp;<a href="https://ssrlab.by">ssrlab.by</a>.
				</p>
				<p class="text-muted">
					<?php echo NgramFrequencyCounter::showMessage('laboratory'), ', ', $ini['year']; if($ini['year'] !== date('Y')) echo '-', date('Y'); ?>
				</p>
			</div>
		</footer>
	</body>
</html>
<?php NgramFrequencyCounter::sendErrorList($lang); ?>